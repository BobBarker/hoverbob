// == Input ==
// PORT0: Sound Signal
// PORT1: FREE
// PORT2: FREE
// PORT3: FREE
// PORT4: FREE
// PORT5: FREE
// PORT6: FREE
// PORT7: FREE

// == Output ==
// PORT0: FREE
// PORT1: FREE
// PORT2: FREE
// PORT3: FREE
// PORT4: FREE
// PORT5: FREE
// PORT6: FREE
// PORT7: FREE

// == Register Usage ===
// EAX: Sound Signal Handler
// EBX: FREE
// ECX: FREE
// EDX: FREE
// ESI: FREE
// EDI: FREE

// == Sound Signal Index ==
DEFINE okayBeepSiginal,1;
DEFINE activateBeepSignal,2;
DEFINE deactivateBeepSignal,3;
DEFINE fireBeepSignal,4;

// == SPU Code Begin ==
MOV EAX,PORT0; // Load Sound Signal
CMP EAX,0; // No sound signal?
 JE endSound; // Then Abort
JMP main;

okayBeep:
 CHPITCH 0,0.4;
 CHSTART 0;
JMP playSound;

activateBeep:
 CHPITCH 0,0.2;
 CHSTART 0;
JMP playSound;

deactivateBeep:
 CHPITCH 0,0.1;
 CHSTART 0;
JMP playSound;

fireBeep:
 CHPITCH 0,0.7;
 CHSTART 0;
JMP playSound;

main:
 WSET 0,trackwave; // Set WAVE 0
 CHWAVE 0,0; // Set CHANNEL 0 to WAVE 0
 CHVOLUME 0,0.3; // Set volume (1 = 0 dB?) for CHANNEL 0

 CMP EAX,okayBeepSiginal;
  JE okayBeep;
 CMP EAX,activateBeepSignal;
  JE activateBeep;
 CMP EAX,deactivateBeepSignal;
  JE deactivateBeep;
 CMP EAX,fireBeepSignal;
  JE fireBeep;
playSound:
 STRING trackwave,"synth/square.wav"; // needs to be at end???
endSound:
 CHSTOP 0; // sound signals will stack up without this
