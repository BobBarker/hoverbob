# HoverBob
## Description
Robot/Drone for Garry's Mod using Wiremod. Written in ZASM while avoiding HL-ZASM and E2. Intended for educational purposes.
## Requirements
* Garry's Mod *http://store.steampowered.com/app/4000*
* Wiremod *https://steamcommunity.com/sharedfiles/filedetails/?id=160250458*
* Advanced Duplicator 2 *https://github.com/wiremod/advdupe2*

## Instructions
* Find your Garry's Mod data directory, normally *C:\Program Files (x86)\Steam\SteamApps\common\GarrysMod\garrysmod\data\*
* Move all hoverBob-CPU#.txt files to *data\cpuship\*
* Move hoverBob-SPU.txt to *data\spuchip\*
* Move hoverBob-AdvDupe2.txt to *data\advdupe2\*
* Use the AdvDupe2 Tool to spawn **hoverBob-AdvDupe2**
* Use the Wiremod CPU Tool to assign **hoverBob-CPU1** to the **left CPU**
* Assign **hoverBob-CPU2** to the **right CPU**
* Use the Wiremod SPU Tool to assign **hoverBob-SPU** to the **SPU**
* Toggle power with the **ignition key**
* Press **RESET**
* Press **ARM**
